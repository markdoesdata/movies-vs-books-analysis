# Movies vs books analysis

This repo contains an R markdown file that was used to analize word frequency and term frequency–inverse document frequency when comparing classic novels and their movie adaptations.  Huckleberry Finn, Romeo and Juliet and the Scarlet Letter were compared. 

## Link to article
[Text Analysis of Classic Novels’ Movie Adaptations](http://mawilson.freeshell.org/writing/mark_wilson-movies-vs-books-analysis.html)

## License
[MIT](https://choosealicense.com/licenses/mit/)
